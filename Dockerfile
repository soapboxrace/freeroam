FROM eclipse-temurin:8u402-b06-jre-alpine

WORKDIR /app

ADD ["sbrw-freeroam-all.jar", "/app/"]

CMD ["java", "-jar", "sbrw-freeroam-all.jar"]
